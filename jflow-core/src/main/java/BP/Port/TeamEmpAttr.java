package BP.Port;

import BP.DA.*;
import BP.Web.*;
import BP.En.*;
import BP.Port.*;
import java.util.*;

/** 
 用户组人员
*/
public class TeamEmpAttr
{
	/** 
	 操作员
	*/
	public static final String FK_Emp = "FK_Emp";
	/** 
	 用户组
	*/
	public static final String FK_Team = "FK_Team";
}